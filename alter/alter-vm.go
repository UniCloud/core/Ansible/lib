/*
* keep: http://libguestfs.org/
* https://serverascode.com/2018/06/26/using-cloud-images.html
*/
func initThis(){
  ansible = '''
  sudo apt install libguestfs-tools -y
  sudo usermod -aG kvm $USER
  '''
}

func info(){
  qemu-img info $img
}

func pass(){
virt-customize -a $img --password me:password:$pass
}

func passRoot(){
virt-customize -a $img --root-password password:$pass
}

func createUser(){
  func simple_works(){
  virt-customize -a $img --run-command 'useradd me'
  }

  func noninteractive(){
  virt-customize -a $img --run-command 'adduser --disabled-password --gecos "" me'
  }
}

func resizeFilesystem(){
qemu-img resize $img 200G
virt-customize -a $img --run-command 'growpart /dev/sda 1'
virt-customize -a $img --run-command 'resize2fs /dev/sda1'

// Test
virt-customize -a $img --run-command 'df -h /'
}

func fstransform(){
/*
* https://fedoramagazine.org/transform-file-systems-in-linux/, http://www.linux-magazine.com/Online/Features/Converting-Filesystems-with-Fstransform
*/
  init(){
  virt-customize -a $img --run-command 'add-apt-repository -y universe'
  virt-customize -a $img --run-command 'apt update'
  virt-customize -a $img --network --install fstransform
  }

  fsInfo(){
  virt-customize -a $img --run-command 'blkid /dev/sda1'
  }

  toBtrfs(){
  # Pre
  virt-customize -a $img --network --install btrfs-progs

  virt-customize -a $img --run-command 'btrfs-convert /dev/sda1'
  }

  toXfs(){
  virt-customize -a $img --network --install xfsprogs
  fstransform /dev/sda1 xfs
  }
}


func info(){
qemu-img info $img
}

func sshSetup(){
virt-customize -a $img --copy-in $file/ssh/sshd_config:/etc/ssh/

regenHostKey(){
/* https://www.cloudvps.com/helpcenter/knowledgebase/linux-vps-configuration/regenerating-ssh-host-keys
*/
virt-customize -a $img --run-command "ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa"
virt-customize -a $img --run-command "ssh-keygen -f /etc/ssh/ssh_host_dsa_key -N '' -t dsa"
}

}

func sshInjectKey(){
// https://www.cyberciti.biz/faq/how-to-add-ssh-public-key-to-qcow2-linux-cloud-images-using-virt-sysprep/

intoUser(){
virt-customize -a $img --mkdir $HOME/.ssh/
virt-customize -a $img --ssh-inject $USER:file:$HOME/.ssh/id_rsa.pub
}

intoRoot(){
virt-customize -a $img --mkdir /root/.ssh/
virt-customize -a $img --ssh-inject root:file:$HOME/.ssh/id_rsa.pub
}
}

func network(){
// https://netplan.io/examples
virt-customize -a $img --copy-in $file/netplan/single.yaml:/etc/netplan/
}

func preAnsible(){
virt-customize -a $img --firstboot-install python
}

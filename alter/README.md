# Home  
http://libguestfs.org

## Guestfish shell
http://libguestfs.org/guestfish.1.html

'All this functionality is available through a scriptable shell called guestfish'

## guestmount
[Mount a guest filesystem on the host using FUSE](http://libguestfs.org/guestmount.1.html)

# languages
[Golang](http://libguestfs.org/guestfs-golang.3.html)  
[Python](http://libguestfs.org/guestfs-python.3.html)  
[Jvm](http://libguestfs.org/guestfs-java.3.html)
